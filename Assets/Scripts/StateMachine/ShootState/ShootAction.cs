using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ShootActions", menuName = "States Machine/Actions/Shoot Machine")]
public class ShootAction : StateAction
{
    Enemy target;
    public override void Init(GameObject owner)
    {
        target = owner.GetComponent<Enemy>();
        target.ResetTimer();
    }

    public override void Run(GameObject owner)
    {
        target.Shoot();
    }
}
