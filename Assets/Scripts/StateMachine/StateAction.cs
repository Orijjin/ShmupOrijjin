using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StateAction : ScriptableObject
{
    public abstract void Init(GameObject owner);
    public abstract void Run(GameObject owner);
    public virtual void Leave(GameObject owner)
    {
        
    }

}
