using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    [SerializeField] PoolItem prefab;
    [SerializeField] int numberObjects;

    private Queue<PoolItem> file;

    void Start()
    {
        file = new Queue<PoolItem>(numberObjects);
        Add(numberObjects);
    }

    public void Add(int quantity = 1)
    {
        for (int i = 0; i < quantity; i++)
        {
            PoolItem obj = Instantiate(prefab, transform);
            file.Enqueue(obj);
            obj.gameObject.SetActive(false);
        }
    }

    public PoolItem Pick()
    {
        if(file.Count == 0)
        {
            Add();
        }
        PoolItem pItem = file.Dequeue();
        pItem.gameObject.SetActive(true);
        pItem.Setup(this);
        return pItem;
    }

    public void Back(PoolItem item)
    {
        item.gameObject.SetActive(false);
        file.Enqueue(item);
    }
}
