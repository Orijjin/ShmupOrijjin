using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Boss", menuName = "Orijjin/boss setup")]
public class SOBoss : SOVaiseauSetup
{
    public List<SOLoot> rewards;
}
