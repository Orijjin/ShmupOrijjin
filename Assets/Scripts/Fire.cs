using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Fire : MonoBehaviour
{
    [SerializeField] Pool poolBullet;
    [SerializeField] Transform shootingPoint;
    [SerializeField] SOVaiseauSetup setup;
    [SerializeField] float level;

    private float deltaFire;
    private bool isFiring;

    void Start()
    {
        
    }

    void Update()
    {
        deltaFire += Time.deltaTime;
        if (isFiring)
        {
            float bps = 1 / setup.getFireRateForLevel(level);
            Debug.Log(bps);
            Debug.Log(setup.getFireRateForLevel(level));
            if (deltaFire >= bps)
            {
                PoolItem bullet = poolBullet.Pick();
                bullet.transform.position = shootingPoint.position;
                bullet.transform.rotation = shootingPoint.rotation;
                deltaFire = 0;
            }
        }
    }

    public void OnShoot(InputAction.CallbackContext ctx)
    {
        isFiring = ctx.ReadValueAsButton();
    }
}
