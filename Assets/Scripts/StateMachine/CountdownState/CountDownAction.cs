using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CountDownActions", menuName = "States Machine/Actions/Countdown Machine")]
public class CountDownAction : StateAction
{
    [HideInInspector] public float timer;

    public override void Init(GameObject owner)
    {
        timer = Time.time;
    }

    public override void Run(GameObject owner)
    {

    }

    public override void Leave(GameObject owner)
    {
        Debug.Log("FINISHED");
    }
}
