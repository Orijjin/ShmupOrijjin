using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateRunner : MonoBehaviour
{
    [SerializeField] BaseState currentState;

    // Start is called before the first frame update
    void Start()
    {
        currentState = ScriptableObject.Instantiate(currentState);
        currentState.Init(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        BaseState nextState = currentState.Run(gameObject);

        if (nextState != null)
            currentState = nextState;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = currentState.stateColor;
        Gizmos.DrawWireSphere(transform.position + Vector3.up, 2.5f);
    }
}
