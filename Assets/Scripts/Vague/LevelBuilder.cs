using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBuilder : MonoBehaviour
{
    [SerializeField] SOLevel level;
    [SerializeField] int startTimer;
    [SerializeField] int betweenTimer;
    [SerializeField] PlayerStats player;
    [SerializeField] Vector2 offsetPlayer = new Vector2(80, 12);

    private int currentVague = -1;
    
    void Start()
    {
        level = ScriptableObject.Instantiate(level);
        level.BuildLevel();
    }

    void Update()
    {
        if(currentVague == -1)
        {
            return;
        }

        SOVague vague = level.Vagues[currentVague];
        if (vague.Progress())
        {
            List<SOVague.VesselGroup> groups = vague.GetVesselReady();
            foreach(SOVague.VesselGroup item in groups)
            {
                foreach(GameObject enemy in item.groupVessel)
                {
                    Vector3 enemyPos = player.transform.position + player.transform.forward * offsetPlayer.x + player.transform.up * Random.Range(-offsetPlayer.y, offsetPlayer.y);
                    GameObject instance = Instantiate(enemy, enemyPos, Quaternion.identity);
                    instance.transform.forward = -player.transform.forward;
                }
            }
        }
    }

    IEnumerator StartGame()
    {
        yield return new WaitForSeconds(startTimer);
        currentVague = 0;
    }
}
