using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerStats : MonoBehaviour
{
    public float maxPv { get; private set; }
    public float pv { get; private set; }
    public float maxAttack { get; private set; }
    [SerializeField] float maxPvBase;
    [SerializeField] float maxAttackBase;
    [Header("HUD")]
    [SerializeField] HealthBar health;

    // Start is called before the first frame update
    void Start()
    {
        maxPv = maxPvBase;
        pv = maxPv;
        maxAttack = maxAttackBase;
        health.SetMaxHelth(maxPv);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void UpdateHUD()
    {

    }

    public void Buy(SOShopItems Item)
    {
        float value = Item.value;
        switch (Item.type)
        {
            case ShopType.MAXPV:
                maxPv += value;
                pv = maxPv;
                health.SetMaxHelth(maxPv);
                break;

            case ShopType.PV:
                pv = Mathf.Min(maxPv, pv + value);
                health.SetHhealth(pv);
                break;

            case ShopType.ATTACK:
                maxAttack = value;
                break;

            case ShopType.SHIELD:
                break;

            case ShopType.SPEED:
                break;
        }
    }
}
