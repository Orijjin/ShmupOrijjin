using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CountDownCond", menuName = "States Machine/Conditions/Countdown Finished")]
public class CountDownCondition : StateCond
{
    public float duration;
    public override bool IsReady(StateAction action, GameObject owner)
    {
        Enemy target = owner.GetComponent<Enemy>();
        return Time.time - target.timer >= duration;
    }
}
