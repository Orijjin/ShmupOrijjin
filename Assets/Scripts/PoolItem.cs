using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolItem : MonoBehaviour
{
    private Pool pool;

    public void Back()
    {
        pool.Back(this);
    }

    public  virtual void Setup(Pool poolBullet)
    {
        pool = poolBullet;
    }
}
