using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerControls : MonoBehaviour
{
    [SerializeField] float speed;
    Vector2 direction;

    void Start()
    {
        
    }

    void Update()
    {
        transform.position += (transform.right * direction.x + transform.forward * direction.y) * speed * Time.deltaTime;
    }

    public void OnMove(InputAction.CallbackContext ctx)
    {
        direction = ctx.ReadValue<Vector2>();
    }
}
