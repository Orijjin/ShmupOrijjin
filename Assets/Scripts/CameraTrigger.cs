using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTrigger : MonoBehaviour
{
    [SerializeField] CinemachineClearShot targetCamera;
    [SerializeField] int targetPriority;

    private void OnTriggerExit(Collider other)
    {
        targetCamera.Priority = targetPriority;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
