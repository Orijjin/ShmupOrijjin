using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Vague", menuName = "Orijjin/vague setup")]
public class SOVague : ScriptableObject
{
    [System.Serializable] public class VesselGroup
    {
        public float timer;
        public float lastTimer { get; set; }
        public List<GameObject> groupVessel;
        public float prob;
    }

    [Range(0, 5)] public int difficulty;
    public List<VesselGroup> group;
    public Vector2 duration;
    public float realDuration { get; private set; }

    public void Build()
    {
        realDuration = Random.Range(duration.x, duration.y); //duration min and max in a vector2
        foreach(VesselGroup item in group)
        {
            item.lastTimer = 0;
        }
    }

    public List<VesselGroup> GetVesselReady()
    {
        List<VesselGroup> res = new List<VesselGroup>();
        foreach (VesselGroup item in group)
        {
            item.lastTimer += Time.deltaTime;
            if(item.lastTimer >= item.timer)
            {
                res.Add(item);
                item.lastTimer = 0;
            }
        }
        return res;
    }

    public bool Progress()
    {
        realDuration -= Time.deltaTime;
        return realDuration > 0;
    }
}
