using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="State", menuName = "States Machine/New State", order = -1)]
public class BaseState : ScriptableObject
{
    [System.Serializable]
    public struct Transition
    {
        public StateCond condition;
        public BaseState nextState;
    }

    public Color stateColor;
    public StateAction action;
    public List<Transition> transitions;

    private bool isInit = false;

    private void InitRef()
    {
        if (isInit)
            return;
        isInit = true;
        action = ScriptableObject.Instantiate(action);
    }

    public void Init(GameObject owner)
    {
        action.Init(owner);
    }

    public BaseState Run(GameObject owner)
    {
        action.Run(owner);

        foreach(Transition tr in transitions)
        {
            if (tr.condition.IsReady(action, owner))
            {
                action.Leave(owner);
                tr.nextState.Init(owner);
                return tr.nextState;
            }
        }

        return null;
    }
}
