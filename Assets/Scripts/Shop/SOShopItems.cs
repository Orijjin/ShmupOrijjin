using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ShopType
{
    MAXPV,
    PV,
    SHIELD,
    ATTACK,
    SPEED
}

[CreateAssetMenu(fileName = "ShopItems", menuName = "Orijjin/shop items")]
public class SOShopItems : ScriptableObject
{
    public string itemLabel;
    public int price;
    public Sprite icon;
    public ShopType type;
    public float value;
}
