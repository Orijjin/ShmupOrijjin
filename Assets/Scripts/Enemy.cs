using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] Pool poolBullet;
    [SerializeField] SOVaiseauSetup setUp;

    public int maxPv { get; private set; }
    public int life { get; private set; }
    public float timer { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        maxPv = setUp.pv;
        life = maxPv;
    }

    private void Update()
    {
        life -= Mathf.RoundToInt(Time.deltaTime * 50);
        //Debug.Log(life);
    }

    public void Fly()
    {
        transform.position += transform.forward * setUp.speed * Time.deltaTime;

    }

    public void Shoot()
    {
        Fly();
    }

    public void ResetTimer()
    {
        timer = Time.time;
    }
}