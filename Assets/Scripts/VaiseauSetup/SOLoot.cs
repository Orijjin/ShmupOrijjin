using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Loot", menuName = "Orijjin/loot setup")]
public class SOLoot : ScriptableObject
{
    public string lootName;
    public Sprite icon;
}
