using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : PoolItem
{
    [SerializeField] float speed;
    [SerializeField] float lifeTime;

    private float curTime;

    void Start()
    {
        
    }

    void Update()
    {
        curTime -= Time.deltaTime;
        transform.position += transform.forward * speed * Time.deltaTime;
        if(curTime <= 0)
        {
            Back();
        }
    }

    public override void Setup(Pool poolBullet)
    {
        curTime = lifeTime;
        base.Setup(poolBullet);
    }

}
