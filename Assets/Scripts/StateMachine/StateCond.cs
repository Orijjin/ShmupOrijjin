using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StateCond : ScriptableObject
{
    public abstract bool IsReady(StateAction action, GameObject owner);
}
