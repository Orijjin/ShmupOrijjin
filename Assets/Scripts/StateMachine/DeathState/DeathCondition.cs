using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DeathCond", menuName = "States Machine/Conditions/Death Condition")]
public class DeathCondition : StateCond
{
    public override bool IsReady(StateAction action, GameObject owner)
    {
        Enemy target = owner.GetComponent<Enemy>();
        return target.life <= 0;
    }
}
