using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "IdleActions", menuName = "States Machine/Actions/Idle Machine")]
public class IdleAction : StateAction
{
    Enemy target;
    public override void Init(GameObject owner)
    {
        target = owner.GetComponent<Enemy>();
        target.ResetTimer();
    }

    public override void Run(GameObject owner)
    {
        target.Fly();
    }
}
