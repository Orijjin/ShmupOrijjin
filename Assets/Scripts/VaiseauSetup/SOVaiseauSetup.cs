using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Vessel", menuName = "Orijjin/vessel setup")]
public class SOVaiseauSetup : ScriptableObject
{
    public float fireRate;
    public int pv;
    public string vasselName;
    public float speed;

    public float getFireRateForLevel(float level)
    {
        return fireRate * level;
    }
}
