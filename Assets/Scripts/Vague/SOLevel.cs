using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "Orijjin/level setup")]
public class SOLevel : ScriptableObject
{
    public int difficulty;
    public List<SOVague> Vagues;
    public List<SOVague> Catalogue;
    public int vagueAmount;

    public void BuildLevel()
    {
        Vagues = new List<SOVague>(vagueAmount);
        for(int i=0; i<vagueAmount; ++i)
        {
            SOVague selection = Catalogue[Random.Range(0, Catalogue.Count)];
            SOVague instance = ScriptableObject.Instantiate(selection);
            Vagues.Add(selection);
            Catalogue.Remove(selection);
       
        }
    }
}
