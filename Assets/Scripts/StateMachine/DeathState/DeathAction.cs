using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DeathActions", menuName = "States Machine/Actions/Death Machine")]
public class DeathAction : StateAction
{
    public override void Init(GameObject owner)
    {
        Destroy(owner);
    }

    public override void Run(GameObject owner)
    {

    }

    public override void Leave(GameObject owner)
    {
        Debug.Log("Explosion");
    }
}
